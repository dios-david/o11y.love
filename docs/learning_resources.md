# Learning Resources

## Newsletters 

Make sure to subscribe and also learn more in their online archive. 

- [o11y.news](https://o11y.news) by [Michael Hausenblas](https://twitter.com/mhausenblas)
- [monitoring weekly](https://monitoring.love/) by [Jason Dixon](https://twitter.com/obfuscurity)

## Talks 

- [All Day DevOps 2021: From Monitoring to Observability: Left Shift your SLOs](https://docs.google.com/presentation/d/1nTpngPtzoOgUu74b1ibDhmxYTwAocxn0/edit)
- [All Day DevOps 2020: From Monitoring to Observability: Migration Challenges from Blackbox to Whitebox](https://docs.google.com/presentation/d/1N5DpAryFp0mSwoyvPge41tjOfc4DszL1/edit)

## Meetups 

- [#EveryoneCanContribute cafe meetup](https://everyonecancontribute.com/)
    - [47. cafe: Observability, quo vadis](https://everyonecancontribute.com/post/2022-01-18-cafe-47-observability-quo-vadis/)
    - [32. cafe: Continuous Profiling with Polar Signals](https://everyonecancontribute.com/post/2021-06-02-cafe-32-polar-signals-continuous-profiling/)
    - [30. cafe: Kubernetes Monitoring with Prometheus](https://everyonecancontribute.com/post/2021-05-19-cafe-30-kubernetes-monitoring-prometheus/)
    - [25. cafe: Observability with Opstrace](https://everyonecancontribute.com/post/2021-04-14-cafe-25-opstrace-observability/)
    - [7. cafe: Docker Hub Rate Limit: Mitigation, Caching and Monitoring](https://everyonecancontribute.com/post/2020-11-04-cafe-7-docker-hub-rate-limit-monitoring/)
    - [6. cafe: Grafana Tempo](https://everyonecancontribute.com/post/2020-10-28-cafe-6-grafana-tempo/)
    - [1. cafe: QuestDB](https://everyonecancontribute.com/post/2020-09-23-cafe-1/)


## Trainings

- [Prometheus Trainings by PromLabs](https://training.promlabs.com/)

## Workshops 

### Practical Kubernetes Monitoring with Prometheus 

The [slides](https://docs.google.com/presentation/d/17XVCbiC4PZYvpreZINmhNltmT-DDCo3LjoaPgqpsxVo/edit) provide a 4+ hours workshop, more details are available [on Michael Friedrich's personal blog](https://dnsmichi.at/2021/09/14/monitoring-kubernetes-with-prometheus-and-grafana-free-workshop/). The following topics will be practiced:

- Monitoring, quo vadis puts the traditional monitoring in contrast to microservices.
- Prometheus and Grafana shares the basic knowledge on Prometheus, PromQL, Service Discovery and terminology required to understand.
- Kubernetes dives into understanding what to monitor, and how.
- Prometheus Operator dives into the concept of the package, and kube-prometheus installing a full stack. You'll dive into the UI of Prometheus, Grafana and the Alert Manager.
- K8s monitoring with Prometheus walks you through the - amazing - default Grafana dashboards, instructs you to deploy a Go demo app with the CRD ServiceMonitor, Container Metrics and kube-state-metrics exercises to practice PromQL queries.
- Advanced Monitoring practices with a Python app and own metrics, deployed to the GitLab container registry and to Kubernetes to query with PromQL in Grafana dashboards. Storage with Thanos/Cortex, Service Discovery is touched as well.
- Alerts and Escalations dives into the Alert Manager and rules, mapped into the PrometheusRule CRD.
- SLA, SLO, SLI keeps you busy with learning about Service Level Objectives for your production environment, providing thoughts on CI/CD quality gates with Keptn - and the OpenSLO spec, Pyrra and Sloth.
- Observability moves from Monitoring to metrics, logs, traces and beyond.
- Secure Monitoring discusses TLS, secret management, Infrastructure as code workflows, Container security and RBAC & policies.
- Ideas on more monitoring with Prometheus exporters, podtato-head, Chaos Engineering, etc.

A shorter version of the workshop [as a talk](https://docs.google.com/presentation/d/1EEBJFgeThlVEeC_E3tOYGicU2X0S9vPGsba49EQSNwk/edit) was provided by Michael Friedrich at PromCon NA 2021, a zero day event at KubeCon NA.

[![](https://img.youtube.com/vi/CyQNYT1ZQQ8/0.jpg)](https://www.youtube.com/watch?v=CyQNYT1ZQQ8 "PromCon NA: Practical Kubernetes Monitoring with Prometheus")
