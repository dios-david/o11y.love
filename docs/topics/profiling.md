# Profiling 

## Introductions

- [Current state of Continuous Profiling](https://o11y.engineering/the-state-of-continuous-profiling-b89cdbdd47f6), Dec 2021

## Tools 

### Parca

> From profiles to understanding. Open Source Infrastructure-wide continuous profiling 

- [Website](https://www.parca.dev/)
- [Documentation](https://www.parca.dev/docs/overview)

#### Facts

- Parca was open-source by [Polar Signals](https://www.polarsignals.com/) in 2021
- Parca's old name was `Conprof` which can be seen in [the KubeCon NA 2021 talk](https://www.youtube.com/watch?v=ficc6_6RYQk)

#### Hot Topics 

- [Feature requests for the Parca agent](https://github.com/parca-dev/parca-agent/issues)




